import unittest
import os
import subprocess
from ifhunter.ipinfo import IpInfo

OVERLAPPING_TEST_ADDRS =\
    [
        {
            'addr': '4.5.0.3',
            'netmask': '255.255.0.0'
        },
        {
            'addr': '4.5.2.1',
            'netmask': '255.255.252.0'
        },
        {
            'addr': '10.50.60.70',
            'netmask': '255.255.0.0'
        },
        {
            'addr': '10.50.60.71',
            'netmask': '255.255.0.0'
        },
        {
            'addr': '127.0.0.1',
            'netmask': '255.0.0.0'
        }
    ]


class TestIpInfo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            TestIpInfo.cont_id = run("dockertest/docker_up").strip()
        except:
            raise AssertionError("docker startup failed, please check your installation")

    @classmethod
    def tearDownClass(cls):
        try:
            run("dockertest/docker_down")
        except:
            raise AssertionError("docker shutdown failed, please check your installation")

    def docker_exec(self, cmd):
        return run("docker exec {0} {1}".format(TestIpInfo.cont_id, cmd))

    def test_get_ips(self):
        result = self.docker_exec("get-ips").splitlines()
        self.assertIn('4.5.0.3', result)
        self.assertIn('10.50.60.70', result)

    def test_get_ips_with_prefix(self):
        result = self.docker_exec("get-ips --with-prefix").splitlines()
        self.assertIn('4.5.0.3/24', result)
        self.assertIn('10.50.60.70/16', result)

    def test_get_ips_overlapping(self):
        # as Docker doesn't allow overlapping subnets, this will return an empty list...
        result = self.docker_exec("get-ips --overlapping").splitlines()
        self.assertEqual([], result)
        # ... so we use an internal test instead, simulating the example mentioned in homework Task 3 section
        ips = IpInfo()
        result = ips.get_ips_overlapping(test_addr_dict=OVERLAPPING_TEST_ADDRS)
        self.assertSetEqual(set(['4.5.0.3/16', '4.5.2.1/22']), set(result))


def run(cmd):
    result = subprocess.run(cmd, universal_newlines=True, shell=True, capture_output=True, check=True)
    return result.stdout


if __name__ == '__main__':
    unittest.main()
