import setuptools


setuptools.setup(
    name='ifhunter',
    version='1.0.0',
    description='Network interface exploration',
    author='Balázs Máthé',
    author_email='mathe.balazs@gmail.com',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7, <4',
    install_requires=[
        'click',
        'netifaces',
        'ipaddress'
    ],
    entry_points={
        'console_scripts': [
            'get-ips=ifhunter.scripts.cli:cli'
        ]
    }
)
