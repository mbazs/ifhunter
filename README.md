# ifhunter

__ifhunter__ is a Python library for acquiring various information from Linux network interfaces.

## Requirements

* Linux OS
* Python 3.7+
* ```docker```
* ```docker-compose```

## Features

 * can detect multiple addresses of an interface
 * can show overlapping subnets

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install __ifhunter__.

```bash
$ git clone https://bitbucket.org/mbazs/ifhunter

# pip install .
```

This installs the tool system-wide from the current directory.

## Usage

### Library

```python
from ifhunter.ipinfo import IpInfo

ips = IpInfo()
ips.get_ips()  # returns a list of strings representing IPv4 addresses of all interfaces
ips.get_ips_with_prefix()  # returns a list of strings representing IPv4 addresses and subnet prefixes of all interfaces 
ips.get_ips_overlapping()  # returns a list of strings representing IPv4 addresses if their subnets overlap
```

### Command line

```bash
$ get-ips
192.168.0.30
1.2.3.4
10.10.0.1
127.0.0.1

$ get-ips --with-prefix
192.168.0.30/24
1.2.3.4/24
10.10.0.1/22
127.0.0.1/8

$ get-ips --overlapping
1.2.3.4/24
10.10.0.1/22
```

## Testing

This project uses [Docker](http://www.docker.com) in order to test network information requests.
It utilizes ```docker-compose``` facility to configure the container in an efficient way. 

You can run the tests with
```
$ ./run_tests
```
in the project directory. Keep in mind that this will launch a Docker container and this will take some time.

### Pitfalls

[Docker](http://www.docker.com) is too smart and won't create network interfaces with overlapping subnets, that's why an inner simulated
test was created for this purpose.

## _+ Task_

* __"Extend the tool to support IPv6 networks"__

I think this mustn't be a great suffer, because the used ```netifaces``` lib correctly handles IPv6 interfaces
 / addresses as well.

* __"Extend the tool to be a daemon that provides the same functionality, but through a RESTful interface"__

Yes it obviously can be done, but somehow I couldn't make it suddenly. Couple of hours required, sorry,
it will work eventually.   

> I've done this before, with Flask it was easy (this is my small project about
> [Geologic Time Scale](https://en.wikipedia.org/wiki/Geologic_time_scale)):
>
> [Archean](https://hjkl.hu/archean) -- The same database in [JSON](https://hjkl.hu/archean/json)
>
> This small API can be queried by such URLs:
>
> * [Phanerozoic JSON](https://hjkl.hu/archean/json?id=phanerozoic)
> * [Meghalayan JSON](https://hjkl.hu/archean/json?id=meghalayan)

So this shouldn't be a big fight. 

* __"Extend the tool with a functionality you think would be useful for the other teams"__

I think hardware address (MAC) printing functionality would be useful, of course together with printing
the vendor from some MAC prefix database previously downloaded.

## License

[MIT](https://choosealicense.com/licenses/mit/)
