import click
from ifhunter.ipinfo import IpInfo


@click.command(help='acquires info about network interfaces')
@click.option('--with-prefix/--no-with-prefix', default=False, help='print addresses with prefixes')
@click.option('--overlapping/--no-overlapping', default=False, help='print overlapping addresses')
def cli(with_prefix, overlapping):
    ips = IpInfo()
    if overlapping:
        for item in ips.get_ips_overlapping():
            print(item)
    elif with_prefix:
        for item in ips.get_ips_with_prefix():
            print(item)
    else:
        for item in ips.get_ips():
            print(item)
