import netifaces as nif
import ipaddress


class IpInfo:
    """Acquires address information from network interfaces"""

    def __init__(self):
        self.ipv4_id = nif.AF_INET

    def __get_ip_addr_dict(self):
        result = []
        for iface in nif.interfaces():
            addrs = nif.ifaddresses(iface)
            if self.ipv4_id in addrs:
                for addr_dict in addrs[self.ipv4_id]:
                    result.append(addr_dict)
        return result

    def __format_addr_with_prefix(self, addr_dict):
        return '{0}/{1}'.format(
            str(ipaddress.ip_address(addr_dict['addr'])),
            str(ipaddress.ip_network('{0}/{1}'.format(addr_dict['addr'], addr_dict['netmask']),
                                     strict=False).prefixlen))

    def get_ips(self):
        """Gets IPv4 addresses from all interfaces

        :returns: a list of strings representing IPv4 addresses of all interfaces
        :rtype: list
        """
        return [str(ipaddress.ip_address(addr_dict['addr'])) for addr_dict in self.__get_ip_addr_dict()]

    def get_ips_with_prefix(self):
        """Gets IPv4 addresses from all interfaces with subnet prefix

        :returns: a list of strings representing IPv4 addresses and subnet prefixes of all interfaces
        :rtype: list
        """
        return [self.__format_addr_with_prefix(addr_dict) for addr_dict in self.__get_ip_addr_dict()]

    def get_ips_overlapping(self, test_addr_dict=None):
        """Gets overlapping IPv4 addresses, i.e. when their subnets overlap

        :returns: a list of strings representing IPv4 addresses if their subnets overlap
        :rtype: list
        """
        addrs = self.__get_ip_addr_dict() if not test_addr_dict else test_addr_dict
        prefixed_addrs = [self.__format_addr_with_prefix(addr) for addr in addrs]
        overlapping_nets = set()
        for i in range(0, len(prefixed_addrs)):
            for j in range(i + 1, len(prefixed_addrs)):
                net0 = ipaddress.ip_network(prefixed_addrs[i], strict=False)
                net1 = ipaddress.ip_network(prefixed_addrs[j], strict=False)
                if net0.overlaps(net1) and net0 != net1:
                    overlapping_nets.add(prefixed_addrs[i])
                    overlapping_nets.add(prefixed_addrs[j])
        return list(overlapping_nets)
